from django.db import models

from touchtechnology.common.db.models import DateTimeField


class TestDateTimeField(models.Model):

    datetime = DateTimeField(blank=True, null=True)

    class Meta:
        ordering = ('datetime',)

    def __unicode__(self):
        return unicode(self.datetime)


class Relative(models.Model):

    name = models.CharField(max_length=10, blank=True, null=True)
    link = models.ForeignKey('TestDateTimeField')
