from django.conf.urls import patterns, include, url

from example_app.sites import (
    TestContextProcessorsSite,
    TestDateTimeFieldSite,
    TestGenericViewsSite,
    TestQueryStringSite,
    TestPaginationSite,
)
from touchtechnology.common.sites import AccountsSite

test_context_processors = TestContextProcessorsSite()
test_date_time_field = TestDateTimeFieldSite()
test_generic_views = TestGenericViewsSite()
test_query_string = TestQueryStringSite()
test_pagination = TestPaginationSite()
accounts = AccountsSite()

urlpatterns = patterns(
    '',
    url(r'^context-processors/', include(test_context_processors.urls)),
    url(r'^date-time-field/', include(test_date_time_field.urls)),
    url(r'^generic/', include(test_generic_views.urls)),
    url(r'^query-string/', include(test_query_string.urls)),
    url(r'^pagination/', include(test_pagination.urls)),
    url(r'^accounts/', include(accounts.urls)),  # can't login without it
    url(r'^', include('touchtechnology.common.urls')),
    url(r'^$', 'example_app.views.index', name='index'),
)
