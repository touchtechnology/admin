#!/bin/bash

export DJANGO_SETTINGS_MODULE=test_app.settings
export PYTHONPATH=$PYTHONPATH:$(pwd)

pushd touchtechnology/admin
django-admin makemessages -l en-AU -l de -l fr -l ja
django-admin compilemessages
popd
