from os.path import dirname, join as basejoin, realpath


def join(*args):
    return realpath(basejoin(*args))


PROJECT_DIR = join(dirname(__file__), '..')

DEBUG = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
    }
}

TIME_ZONE = 'Australia/Sydney'
LANGUAGE_CODE = 'en-AU'

USE_TZ = True
STATIC_URL = '/static/'

STATIC_ROOT = join(PROJECT_DIR, 'static')
MEDIA_ROOT = join(PROJECT_DIR, 'media')

SECRET_KEY = '2bksb4rhbv7i1$!5xzux0&amp;&amp;sl2@de@k6sxb4(zlj4l)+xds#2i'
SITE_ID = 1

ROOT_URLCONF = 'test_app.urls'
WSGI_APPLICATION = 'test_app.wsgi.application'

SESSION_ENGINE = 'django.contrib.sessions.backends.signed_cookies'

INSTALLED_APPS = (
    'mptt',
    'guardian',

    'touchtechnology.common',
    'touchtechnology.admin',
    'example_app',

    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sites',
    'django.contrib.sessions',

    'bootstrap3',
    'django_gravatar',
)

FIXTURE_DIRS = (
    join(PROJECT_DIR, 'fixtures'),
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'guardian.backends.ObjectPermissionBackend',
)

ANONYMOUS_USER_ID = -1
ANONYMOUS_DEFAULT_USERNAME_VALUE = 'anonymous'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'debug': DEBUG,
            'context_processors': (
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'touchtechnology.common.context_processors.env',
                'touchtechnology.common.context_processors.query_string',
                'touchtechnology.common.context_processors.site',
                'touchtechnology.common.context_processors.tz',
            ),
        },
    },
]

MIDDLEWARE_CLASSES = (
    'django.contrib.sites.middleware.CurrentSiteMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'touchtechnology.common.middleware.TimezoneMiddleware',
)
