from django.conf.urls import include, url
from django.contrib.admin import autodiscover
from touchtechnology.common.sites import AccountsSite

from touchtechnology.admin import sites as admin

autodiscover()
accounts = AccountsSite()

urlpatterns = [
    url(r'^accounts/', include(accounts.urls)),
    url(r'^admin/', include(admin.site.urls)),
]
