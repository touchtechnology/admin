from django.core.urlresolvers import reverse
from django.test import TestCase


class AuthenticationTest(TestCase):

    fixtures = ['user']

    def test_admin_login(self):
        url = reverse('admin:index')
        redirect_to = reverse('accounts:login') + '?next=' + url
        res = self.client.get(url)
        self.assertRedirects(res, redirect_to)

    def test_admin_logout(self):
        res = self.client.login(username='gary', password='password')
        self.assertTrue(res)

        # executing the test_admin_login case should fail because we are
        # authenticated in this context
        with self.assertRaises(AssertionError):
            self.test_admin_login()

        # invoke the logout url and check the response
        text = """
        <p>You have successfully been logged out.</p>
        """
        url = reverse('accounts:logout')
        res = self.client.get(url)
        self.assertContains(res, text, html=True)

        # executing the test_admin_login case now works because we're no
        # longer authenticated following our logout
        self.test_admin_login()
