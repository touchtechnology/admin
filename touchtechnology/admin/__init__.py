import logging

logger = logging.getLogger(__name__)

NAME = 'Administration System'
INSTALL = ()

__version__ = '3.3.0'

logger.debug('"%s"/"%s"' % (NAME, __version__))

default_app_config = 'touchtechnology.admin.apps.AdminConfig'
