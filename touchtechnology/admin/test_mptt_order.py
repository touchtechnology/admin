from django.core.urlresolvers import reverse
from django.test import TestCase

from touchtechnology.common.models import SitemapNode


class SitemapMoveTest(TestCase):

    fixtures = ['user', 'sitemapnode']

    def test_move_down_up(self):
        # baseline for the mptt values of the nodes, order by pk to ensure
        # subsequent check is valid
        before_move = list(SitemapNode.objects.order_by('pk').values(
            'pk', 'lft', 'rght', 'tree_id'))

        # determine url to move first node down, and expected redirection
        url = reverse('admin:reorder', args=(1, 'down'))
        redirect_to = reverse('admin:index')

        # perform the move using the admin view
        self.client.login(username='gary', password='password')
        res = self.client.get(url, follow=False)
        self.assertRedirects(res, redirect_to)

        # after moving the node down make sure the mptt values are different
        after_move_1 = list(SitemapNode.objects.order_by('pk').values(
            'pk', 'lft', 'rght', 'tree_id'))
        self.assertNotEqual(before_move, after_move_1)

        # try moving the node back up
        url = reverse('admin:reorder', args=(1, 'up'))
        res = self.client.get(url, follow=False)
        self.assertRedirects(res, redirect_to)

        # after moving the node up the mptt values should be restored
        after_move_2 = list(SitemapNode.objects.order_by('pk').values(
            'pk', 'lft', 'rght', 'tree_id'))
        self.assertNotEqual(after_move_1, after_move_2)
        self.assertEqual(before_move, after_move_2)
