from django.contrib.auth.models import Group

from test_plus.test import TestCase as PlusTestCase


class TestCase(PlusTestCase):

    def response_405(self, response=None):
        """ Given response has status_code 405 """
        response = self._which_response(response)
        self.assertEqual(response.status_code, 405)

    def get_generic_200(self, url_name, username, password, in_context,
                        template, **kw):
        self.assertLoginRequired(url_name, **kw)
        with self.login(username=username, password=password):
            res = self.get(url_name, **kw)
            self.response_200()
            for key in in_context:
                self.assertInContext(key)
            self.assertTemplateUsed(res, template)


class AuthSiteUserTests(TestCase):

    fixtures = ['user']

    def test_index(self):
        url_name = 'admin:auth:index'

        self.assertLoginRequired(url_name)

        with self.login(username='gary', password='password'):
            self.get(url_name)
            self.response_302()

    def test_user_list(self):
        self.get_generic_200(
            'admin:auth:users:list',
            'gary', 'password',
            ['model', 'object_list'],
            'touchtechnology/admin/list.html',
        )

    def test_user_create(self):
        self.get_generic_200(
            'admin:auth:users:add',
            'gary', 'password',
            ['model', 'form'],
            'touchtechnology/admin/edit.html',
        )

    def test_user_edit(self):
        self.get_generic_200(
            'admin:auth:users:edit',
            'gary', 'password',
            ['model', 'form', 'object'],
            'touchtechnology/admin/edit.html',
            pk=1,
        )

    def test_user_perms(self):
        self.get_generic_200(
            'admin:auth:users:perms',
            'gary', 'password',
            ['model', 'formset'],
            'touchtechnology/admin/permissions.html',
            pk=1,
        )

    def test_user_delete(self):
        url_name = 'admin:auth:users:delete'

        with self.login(username='gary', password='password'):
            self.get(url_name, pk=1)
            self.response_405()

            self.post(url_name, pk=1)
            self.response_302()


class AuthSiteGroupTests(TestCase):

    fixtures = ['user']

    def setUp(self):
        self.group_instance = Group.objects.create(name='Group')

    def test_group_list(self):
        self.get_generic_200(
            'admin:auth:groups:list',
            'gary', 'password',
            ['model', 'object_list'],
            'touchtechnology/admin/list.html',
        )

    def test_group_create(self):
        self.get_generic_200(
            'admin:auth:groups:add',
            'gary', 'password',
            ['model', 'form'],
            'touchtechnology/admin/edit.html',
        )

    def test_group_edit(self):
        self.get_generic_200(
            'admin:auth:groups:edit',
            'gary', 'password',
            ['model', 'form', 'object'],
            'touchtechnology/admin/edit.html',
            pk=self.group_instance.pk,
        )

    def test_group_perms(self):
        self.get_generic_200(
            'admin:auth:groups:perms',
            'gary', 'password',
            ['model', 'formset'],
            'touchtechnology/admin/permissions.html',
            pk=self.group_instance.pk,
        )

    def test_group_delete(self):
        url_name = 'admin:auth:groups:delete'

        with self.login(username='gary', password='password'):
            self.get(url_name, pk=self.group_instance.pk)
            self.response_405()

            self.post(url_name, pk=self.group_instance.pk)
            self.response_302()
