from django.apps import AppConfig
from django.conf import settings
from django.core import checks
from first import first


class AdminConfig(AppConfig):
    name = 'touchtechnology.admin'
    label = 'touchtechnology_admin'


@checks.register('ecosystem')
def touchtechnology_assumptions(app_configs, **kwargs):
    """
    When building a touchtechnology.admin driven piece of functionality, we
    make plenty of opinionated assumptions about what will be happening. So we
    don't shoot ourselves in the foot too often, these checks should be run to
    ensure we are following our conventions.
    """
    errors = []

    # determine if we are running a multi-tenanted installation or not
    multitenant = 'tenant_schemas' in settings.INSTALLED_APPS
    sites = 'django.contrib.sites' in settings.INSTALLED_APPS

    if not multitenant and not sites:
        errors.append(checks.Critical(
            "touchtechnology.admin requires either 'django-tenant-schemas' or "
            "'django.contrib.sites' to be in INSTALLED_APPS",
            hint="Add either 'tenant_schemas' or 'django.contrib.sites' to "
                 "INSTALLED_APPS",
        ))

    # ensure appropriate tenant/site middleware is first loaded
    first_middleware = first(settings.MIDDLEWARE_CLASSES)

    if multitenant:
        if first_middleware not in (
                'tenant_schemas.middleware.TenantMiddleware',
                'tenant_schemas.middleware.SuspiciousTenantMiddleware'):
            errors.append(checks.Critical(
                "touchtechnology.admin requires a 'django-tenant-schemas' "
                "middleware to be listed first.",
                hint=None,
            ))
    elif sites:
        if first_middleware not in (
                'django.contrib.sites.middleware.CurrentSiteMiddleware',):
            errors.append(checks.Critical(
                "touchtechnology.admin requires "
                "'django.contrib.sites.middleware.CurrentSiteMiddleware' to "
                "be listed first.",
                hint=None,
            ))
    else:
        errors.append(checks.Warning(
            "Make sure the appropriate middleware is listed first.",
            hint=None,
        ))

    return errors
