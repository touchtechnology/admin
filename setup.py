from setuptools import setup, find_packages

setup(
    name='touchtechnology-admin',
    version='3.3.0',
    author='Touch Technology Pty Ltd',
    author_email='support@touchtechnology.com.au',
    url='https://bitbucket.org/touchtechnology/admin/',
    description='Administrative interface for Touch Technology CMS.',
    packages=find_packages(exclude=["test_app"]),
    install_requires=[
        'touchtechnology-common>=3.5.2,<3.6',
        'django-bootstrap3>=7.0,<8',
        'django-gravatar2>=1.4,<2',
    ],
    tests_require=[
        'pytz==2013.7',
    ],
    include_package_data=True,
    namespace_packages=['touchtechnology'],
    zip_safe=False,
)
